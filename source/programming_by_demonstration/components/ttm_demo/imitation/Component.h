/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    programming_by_demonstration::ArmarXObjects::ttm_demo_imitation
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <mutex>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/util/tasks.h>

// #include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

// #include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/interface/objectpose/ObjectPoseProvider.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>
#include <RobotAPI/interface/core/RobotLocalization.h>
#include <programming_by_demonstration/components/ttm_demo/imitation/ComponentInterface.h>
#include <armar6/skills/interface/PickAndPlaceComponentInterface.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>
#include <RobotAPI/libraries/armem/client/plugins.h>
#include <armar6/skills/libraries/Armar6Tools/Armar6HandV2HelperSimple.h>
#include "RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h"
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>


namespace armarx::programming_by_demonstration::components::ttm_demo::imitation
{

    class Component :
        virtual public armarx::Component,
        virtual public armarx::programming_by_demonstration::components::ttm_demo::imitation::ComponentInterface,
        virtual public armarx::ObjectPoseClientPluginUser,
        // , virtual public armarx::DebugObserverComponentPluginUser
        virtual public armarx::LightweightRemoteGuiComponentPluginUser,
        virtual public armarx::armem::client::ComponentPluginUser,
        virtual public armarx::RobotUnitComponentPluginUser
        // , virtual public armarx::ArVizComponentPluginUser
    {
    public:

        Component();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();

        void start(const ttm_demo::imitation::ExecuteRequest& request, const Ice::Current&) override;

        void executeGrasp(const Ice::Current&) override;

        void stop(const Ice::Current&) override;

    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;

    private:

        void activateImpedanceController(const std::string& rnsName);

        void run();

        void graspAndLift(grasping::GraspCandidatePtr candidate);



        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */

    private:

        static const std::string defaultName;

        // Private member variables go here.

        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            IceUtil::Time startupTimeout = IceUtil::Time::seconds(1);
            IceUtil::Time objectRequestPeriod = IceUtil::Time::seconds(1);
            IceUtil::Time objectSearchTimeout = IceUtil::Time::seconds(5);
            IceUtil::Time retreatTimeout = IceUtil::Time::seconds(1);
            bool dryRun = false;
            int placingPosReachedThreshold = 70;
            Eigen::Vector3f impedanceKpos{400, 400, 300};
            Eigen::Vector3f impedanceDpos{400, 400, 300};
            float impedanceApproachLinearVel = 60;
            float impedanceRetreatLinearVel = 120;
            float approachHaldClosure = 0.2;
        };
        Properties properties;
        std::mutex propertiesMutex;

        struct RemoteProcedureCall
        {
            objpose::ObjectPoseProviderPrx objectPoseProvider;
            grasping::PickAndPlaceComponentInterfacePrx pickAndPlace;
            armarx::HandUnitInterfacePrx rightHandUnit;
        };
        RemoteProcedureCall rpc;

        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::IntSpinBox startupTimeout;
            armarx::RemoteGui::Client::IntSpinBox objectRequestPeriod;
            armarx::RemoteGui::Client::IntSpinBox objectSearchTimeout;
            armarx::RemoteGui::Client::IntSpinBox retreatTimeout;

        };
        RemoteGuiTab tab;

        armarx::armem::robot_state::VirtualRobotReader robotReader;
        VirtualRobot::RobotPtr robot;

        std::unique_ptr<armarx::simple::Armar6HandV2Helper> rightHandHelper;

        armarx::NJointTaskSpaceImpedanceControlInterfacePrx taskSpaceImpedanceController;

        ttm_demo::imitation::ExecuteRequest request;
        armarx::RunningTask<Component>::pointer_type task;
        armarx::SimplePeriodicTask<>::pointer_type syncRobotTask;

        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */

    };

}  // namespace armarx::programming_by_demonstration::components::ttm_demo_imitation
