/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    programming_by_demonstration::bimanual_action_recognition
 * author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * date       2022
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License
 */


#pragma once


#include <RobotAPI/interface/armem/client/MemoryListenerInterface.ice>


module armarx {  module programming_by_demonstration {  module components {  module ttm_demo { module rule_based_ar
{

    struct Segment
    {
        long beginTime;
        long endTime;
        string label;
    };

    sequence<Segment> Segments;

    struct Segmentation
    {
        long referenceTime;
        Segments segments;
    };

    interface ComponentInterface extends
        armarx::armem::client::MemoryListenerInterface  // Needed because of ListenerPlugin.
    {
        void start();
        Segmentation stop();
    };

};};};};};
