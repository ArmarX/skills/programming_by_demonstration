/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    programming_by_demonstration::ArmarXObjects::rule_based_biar
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// #include <mutex>

#include <ArmarXCore/core/Component.h>

// #include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

// #include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

// #include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <programming_by_demonstration/components/ttm_demo/rule_based_ar/ComponentInterface.h>

#include <RobotAPI/libraries/armem/client/plugins/ListeningPluginUser.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <ArmarXCore/util/tasks.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>


namespace armarx::programming_by_demonstration::components::rule_based_biar
{

    class Component :
        virtual public armarx::Component,
        virtual public programming_by_demonstration::components::ttm_demo::rule_based_ar::ComponentInterface,
        virtual public armarx::armem::ListeningClientPluginUser
        // , virtual public armarx::DebugObserverComponentPluginUser
        // , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        // , virtual public armarx::ArVizComponentPluginUser
    {

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();

        void start(const Ice::Current&) override;

        ttm_demo::rule_based_ar::Segmentation stop(const Ice::Current&) override;

    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /* (Requires armarx::LightweightRemoteGuiComponentPluginUser.)
        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;
        */

        void processObjectUpdates(const armem::MemoryID& subscriptionID, const std::vector<armem::MemoryID>& snapshotIDs);


    private:

        // Private methods go here.

        void run();

        struct ReadMemoriesResult
        {
            armem::wm::Memory objects;
            armem::wm::Memory humanPoses;
        };

        armem::Time getCurrentTime();

        ReadMemoriesResult readMemories(armem::Time time);

        void updateMemory(const std::string& action);

        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */


    private:

        static const std::string defaultName;

        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            bool referenceTimeEnabled = false;
            long referenceTime = -1;
            armem::MemoryID objectInstances{"Object/Instance"};
            armem::MemoryID humanPoses{"Human/Pose"};
            armem::MemoryID recognizedActions{"Human/RecognizedAction"};
            int contactBelow = 200;
            int releasedAbove = 300;
        };
        Properties properties;

        ttm_demo::rule_based_ar::Segment currentSegment;
        ttm_demo::rule_based_ar::Segmentation segmentation;

        armem::Time timeOffset;

        /* Use a mutex if you access variables from different threads
         * (e.g. ice functions and RemoteGui_update()).
        std::mutex propertiesMutex;
        */

        armem::client::Reader objectInstanceMemory;
        armem::client::Reader humanPoseMemory;
        armem::client::Writer recognizedActionsMemory;

        armarx::RunningTask<Component>::pointer_type task;

        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::LineEdit boxLayerName;
            armarx::RemoteGui::Client::IntSpinBox numBoxes;

            armarx::RemoteGui::Client::Button drawBoxes;
        };
        RemoteGuiTab tab;
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */

    };

}  // namespace armarx::programming_by_demonstration::components::rule_based_biar
