/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    programming_by_demonstration::ArmarXObjects::pbd_cycle
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <mutex>

#include <ArmarXCore/core/Component.h>

// #include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

// #include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <programming_by_demonstration/components/ttm_demo/pbd_cycle/ComponentInterface.h>
#include <programming_by_demonstration/components/ttm_demo/imitation/ComponentInterface.h>
#include <programming_by_demonstration/components/ttm_demo/rule_based_ar/ComponentInterface.h>


namespace armarx::programming_by_demonstration::components::pbd_cycle
{

    class Component :
        virtual public armarx::Component,
        virtual public armarx::programming_by_demonstration::components::pbd_cycle::ComponentInterface
        // , virtual public armarx::DebugObserverComponentPluginUser
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        // , virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();

    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;

    private:

        // Private methods go here.

        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */

    private:

        static const std::string defaultName;

        // Private member variables go here.

        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            std::string actions = "lift Kitchen/corny-cereal-bars, "
                                  "place Kitchen/corny-cereal-bars, "
                                  "lift Kitchen/soy-milk, "
                                  "place Kitchen/soy-milk, "
                                  "lift KIT/Rusk, "
                                  "place KIT/Rusk";
            std::string meetsConstraints = "lift Kitchen/soy-milk > place Kitchen/soy-milk,"
                                           "lift Kitchen/corny-cereal-bars > place Kitchen/corny-cereal-bars, "
                                           "lift KIT/Rusk > place KIT/Rusk";
            std::string beforeConstraints = "lift Kitchen/soy-milk > lift KIT/Rusk, "
                                            "lift Kitchen/soy-milk > place KIT/Rusk, "
                                            "place Kitchen/soy-milk > lift KIT/Rusk, "
                                            "place Kitchen/soy-milk > place KIT/Rusk";
        };
        Properties properties;
        std::mutex propertiesMutex;

        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::LineEdit actions;
            armarx::RemoteGui::Client::LineEdit meets;
            armarx::RemoteGui::Client::LineEdit before;

            armarx::RemoteGui::Client::Button startActionRecognition;
            armarx::RemoteGui::Client::Button stopActionRecognition;
            armarx::RemoteGui::Client::Button startImitation;
            armarx::RemoteGui::Client::Button stopImitation;
        };
        RemoteGuiTab tab;

        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */

        ttm_demo::rule_based_ar::ComponentInterfacePrx ar;
        ttm_demo::imitation::ComponentInterfacePrx imitation;

    };

}  // namespace armarx::programming_by_demonstration::components::pbd_cycle
