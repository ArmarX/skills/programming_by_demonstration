/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "aron_conversions.h"


namespace armarx::programming_by_demonstration::core
{

    void
    toAron(arondto::Action& dto, const Action& bo)
    {
        dto.name = bo.name;
    }

    void
    fromAron(const arondto::Action& dto, Action& bo)
    {
        bo.name = dto.name;
    }

} // namespace armarx::programming_by_demonstration::core
